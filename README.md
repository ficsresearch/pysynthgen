# pySynthGen

*A python port of synth gen by Sarah Amir*

## Installation

`pysynthgen` can be installed by downloading from www.trust-hub.org or using git with:

```bash
$ git clone https://gitlab.com/ficsresearch/pysynthgen.git
```

```bash
$ cd /path/to/pysynthgen
$ ls
LICENSE     
README.md   
__init__.py 
matlab_src  
output      
setup.py    
src
tester.py

$ pip install -e .

```

## Usage
Installation of the package exposes the command line utility as `pysynth`

```bash
$ pysynth -h
 © Copyright 2021 University of Florida Research Foundation, Inc. All Rights Reserved.
 This program comes with ABSOLUTELY NO WARRANTY;
 This is free software, and you are welcome to redistribute it under certain conditions;

usage: pysynthgen [-h] [-sd SAVEDIR] [-bn BENCHNAME] [-bi BENCHID] [-nb NUMBENCH] [-n NUMNODES] [-nr NUMNODESRANGE] [-d DEPTH] [-in INPUTS]
                  [-inr INPUTSRANGE] [-out OUTPUTS] [-outr OUTPUTSRANGE] [-t TIMEOUT] [-mfi MAXFANIN] [-mfo MAXFANOUT] [-s {min,max}]
                  [-ot {verilog,gsc,bench,all}] [-ips] [-f] [--debug] [--debugMatPath DEBUGMATPATH] [-gw [GATEWEIGHTS [GATEWEIGHTS ...]]]

optional arguments:
  -h, --help            show this help message and exit
  -sd SAVEDIR, --saveDir SAVEDIR
                        Directory to save synthesized designs
  -bn BENCHNAME, --benchName BENCHNAME
                        set a name for the benchmark
  -bi BENCHID, --benchId BENCHID
                        Identifier for configuration (if numBench is greater than 1, ids start with this)
  -nb NUMBENCH, --numBench NUMBENCH
                        Number of benchmarks to create with given parameters
  -n NUMNODES, --numNodes NUMNODES
                        Number of gates in benchmark
  -nr NUMNODESRANGE, --numNodesRange NUMNODESRANGE
                        Randomly selects integers within this range for number of nodes in a generated benchmark. Overrides --numNodes.
  -d DEPTH, --depth DEPTH
                        Depth of the bench to synthesize
  -in INPUTS, --inputs INPUTS
                        Specify number of inputs for benchmark(s)
  -inr INPUTSRANGE, --inputsRange INPUTSRANGE
                        Randomly selects integers within this range for number of inputs in a generated benchmark. Overrides --inputs.
  -out OUTPUTS, --outputs OUTPUTS
                        Specify number of outputs for benchmark(s)
  -outr OUTPUTSRANGE, --outputsRange OUTPUTSRANGE
                        Randomly selects integers within this range for number of outputs in a generated benchmark. Overrides --outputs.
  -t TIMEOUT, --timeout TIMEOUT
                        Timeout for solution.
  -mfi MAXFANIN, --maxFanin MAXFANIN
                        Max fan in of nodes
  -mfo MAXFANOUT, --maxFanout MAXFANOUT
                        Max fan out of nodes
  -s {min,max}, --sense {min,max}
                        Optimization direction.
  -ot {verilog,gsc,bench,all}, --outType {verilog,gsc,bench,all}
                        Output file types. See NetlistType for more info.
  -ips, --ignorePrevSol
                        By default, solutions look for previous solutions of same name and include in constraints to ensure varying solutions
                        for same params. Can add this flag to ignore previous solutions.
  -f, --force           Force Synthesizer to solve LP even if solved file already exists (overwrites existing solution)
  --debug               Debugging utility to compare solutions to MATLAB solutions. Forces all default parameters.
  --debugMatPath DEBUGMATPATH
                        Path to debug MAT file to test matrices and constraints against. Created by saving entire MATLAB environment after
                        running SynthGen. By default, uses included tests/debug.mat file.
  -gw [GATEWEIGHTS [GATEWEIGHTS ...]], --gateWeights [GATEWEIGHTS [GATEWEIGHTS ...]]
                        Specify integer weights for likelihood of gate insertion
```





