#  Copyright (c) University of Florida 2022.
#
#  Author: Daniel Capecci
#
#  This file is a part of PySynthGen - a python port of SynthGen by Sarah Amir.
#
#  PySynthGen is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pysynthgen",
    version="1.0.0",
    author="Daniel Capecci",
    author_email="dcapecci@ufl.edu",
    description="Python port of SynthGen",
    long_description=long_description,
    long_description_content_type="text/markdown",
    # url="",
    # project_urls={},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src", exclude=['tests', 'matlab_src']),
    include_package_data=True,
    install_requires=[
        'mip',
        'jsonpickle',
        'tqdm',
        'numpy',
        'pyyaml',
        'scikit-learn',
        'scikit-learn-intelex',
        'scipy',
    ],
    entry_points={
        'console_scripts': ['pysynth=pysynthgen.cli_main:main']
    },
    python_requires=">=3.8",
)
