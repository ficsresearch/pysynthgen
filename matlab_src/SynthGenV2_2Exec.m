%% Copyright and Licence

% ©Copyright 2020 University of Florida Research Foundation, Inc. All Rights Reserved.
% 
% This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation version 3
%
% This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

function SynthGenV2Exec(directory,Sample_Number,nodeSize,depth,Inputs,Outputs,Timeout)
% Files needed to be in the same directory: 
%                     ccircStats2.m 
%                     genStats.m
%                     PadStats.m
%                     ReferenceBenchmarks/<ref_files>.stats    

% Process: PCA -> Hypothetical -> Reverse PCA -> Normalized -> Optimize
if(~isnumeric(Sample_Number))
    Sample_Number = str2num(Sample_Number);
    nodeSize= str2num(nodeSize);
    depth = str2num(depth);
    Inputs = str2num(Inputs);
    Outputs = str2num(Outputs);
    Timeout = str2num(Timeout);
end

%% Settings

MinMax = 1; % Enter -1 for maximization, 1 for minimization of distance
showplot = false;

% clearvars; clc; %close all; 
% directory = '/home/UFAD/sarah.amir/SyntheticBenchmark/Extension/';
% Sample_Number = 1 ;
% nodeSize= 500; %% (total number of gates)
% depth = 50; %% (depth or delay)
% Inputs = 50;
% Outputs = 20;
maxFanin = 3;
maxFanout = 3;

disp('©Copyright 2020 University of Florida Research Foundation, Inc. All Rights Reserved.');
disp('This program comes with ABSOLUTELY NO WARRANTY;');
disp('This is free software, and you are welcome to redistribute it under certain conditions;');

%% DEFINITIONS %%
% 
%                    1 :         n           = newNode
%          n        +1 :        2n           = newInput         
%         2n        +1 :        3n           = newOutput
%         3n        +1 :        4n           = newPO
%         4n        +1 :        5n           = newEdge
%         5n        +1 :        6n           = newFanout
%         6n        +1 :  n^2 + 6n           = Edge Table
%   n^2 + 6n        +1 : 2n^2 + 6n           = Fanout Table
%  2n^2 + 6n        +1 : 3n^2 + 6n           = Fanin Table
%  3n^2 + 6n        +1 : 3n^2 + 7n           = Non-equality binary parameters
%  3n^2 + 7n +      +1 : 3n^2 + 7n +   mn    = Difference(Node(1:m))
%  3n^2 + 7n +   mn +1 : 3n^2 + 7n +  2mn    = Difference(Input(1:m))
%  3n^2 + 7n +  2mn +1 : 3n^2 + 7n +  3mn    = Difference(Output(1:m))
%  3n^2 + 7n +  3mn +1 : 3n^2 + 7n +  4mn    = Difference(PO(1:m))
%  3n^2 + 7n +  4mn +1 : 3n^2 + 7n +  5mn    = Difference(Edge(1:m))
%  3n^2 + 7n +  5mn +1 : 3n^2 + 7n +  6mn    = Difference(Fanout(1:m))
%  3n^2 + 7n +  6mn +1 : 3n^2 + 7n +  7mn    = Absolute(Difference(Node(1:m)))
%  3n^2 + 7n +  7mn +1 : 3n^2 + 7n +  8mn    = Absolute(Difference(Input(1:m)))
%  3n^2 + 7n +  8mn +1 : 3n^2 + 7n +  9mn    = Absolute(Difference(Output(1:m)))
%  3n^2 + 7n +  9mn +1 : 3n^2 + 7n + 10mn    = Absolute(Difference(PO(1:m)))
%  3n^2 + 7n + 10mn +1 : 3n^2 + 7n + 11mn    = Absolute(Difference(Edge(1:m)))
%  3n^2 + 7n + 11mn +1 : 3n^2 + 7n + 12mn    = Absolute(Difference(Fanout(1:m)))
%  3n^2 + 7n + 12mn +1 : 3n^2 + 7n + 13mn    = Absolution binary parameter(Node(1:m))
%  3n^2 + 7n + 13mn +1 : 3n^2 + 7n + 14mn    = Absolution binary parameter(Input(1:m))
%  3n^2 + 7n + 14mn +1 : 3n^2 + 7n + 15mn    = Absolution binary parameter(Output(1:m))
%  3n^2 + 7n + 15mn +1 : 3n^2 + 7n + 16mn    = Absolution binary parameter(PO(1:m))
%  3n^2 + 7n + 16mn +1 : 3n^2 + 7n + 17mn    = Absolution binary parameter(Edge(1:m))
%  3n^2 + 7n + 17mn +1 : 3n^2 + 7n + 18mn    = Absolution binary parameter(Fanout(1:m))
%

%% READING REFERENCE %%
% as the first depth represents primary inputs, data dimention is n=depth+1
n = depth +1; 
depth = n; % in supplementary files

% initializing variable
Nodeall(1,:) = zeros(1,depth); POall(1,:) = zeros(1,depth); Fanoutall(1,:) = zeros(1,depth);
Edgeall(1,:) = zeros(1,depth); Inputall(1,:) = zeros(1,depth); Outputall(1,:) = zeros(1,depth);
% Latchedall(1,:) = zeros(1,depth);

iscas = ["c1355"; "c17"; "c1908"; "c2670"; "c3540"; "c432"; "c499"; "c5315"; "c6288"; "c7552"; "c880"];
epfl = ["arbiter"; "bar"; "cavlc"; "ctrl"; "dec"; "div"; "hyp"; "i2c"; "int2float"; "log2"; "max"; "multiplier"; "priority"; "router"; "sin"; "sqrt"; "square"; "voter"];
mcnc = ["ex7"; "al2"; "example2"; "o64"; "alcom"; "opa"; "alu1"; "f5xp1"; "p82"; "alu2"; "frg1"; "pair"; "alu4"; "frg2"; "parity"; "amd"; "pcler8"; "apex1"; "i1"; "pcle"; "apex2"; "i2"; "pm1"; "apex3"; "i3"; "pope"; "apex4"; "i4"; "prom1"; "apex5"; "i5"; "prom2"; "apex6"; "i6"; "rd53"; "apex7"; "i7"; "rd73"; "i8"; "rd84"; "i9"; "risc"; "ibm"; "root"; "in0"; "rot"; "bc0"; "in1"; "ryy6"; "br1"; "in2"; "s9sym"; "br2"; "in3"; "sao2"; "c1355"; "in4"; "sct"; "c17"; "in5"; "seq"; "c1908"; "in6"; "sex"; "c2670"; "in7"; "shift"; "c3540"; "intb"; "signet"; "c432"; "jbp"; "soar"; "c499"; "k2"; "sqn"; "c5315"; "lal"; "sqr6"; "c6288"; "lin"; "sqrt8"; "c7552"; "luc"; "squar5"; "c880"; "m1"; "t1"; "c8"; "m2"; "t3"; "cc"; "m3"; "t481"; "chkn"; "m4"; "table3"; "cht"; "mainpla"; "table5"; "clip"; "majority"; "tcon"; "max1024"; "term1"; "cm138a"; "max128"; "ti"; "cm150a"; "max46"; "tms"; "cm151a"; "max512"; "too_large"; "cm152a"; "misex1"; "ts10"; "cm162a"; "misex2"; "ttt2"; "cm163a"; "misex3"; "cm42a"; "misg"; "unreg"; "cm82a"; "mish"; "vda"; "cm85a"; "misj"; "vg2"; "cmb"; "mlp4"; "vtx1"; "comp"; "mp2d"; "x1dn"; "con1"; "mux"; "x1"; "cordic"; "my_adder"; "x2dn"; "count"; "newapla1"; "x2"; "cps"; "newapla2"; "x3"; "cu"; "newapla"; "x4"; "dalu"; "newbyte"; "x6dn"; "dc1"; "newcond"; "x7dn"; "dc2"; "newcpla1"; "x9dn"; "decod"; "newcpla2"; "xor5"; "des"; "newcwp"; "xparc"; "dist"; "newill"; "duke2"; "newtag"; "Z5xp1"; "e64"; "newtpla1"; "Z9sym"; "ex4"; "newtpla2"; "ex5"; "newtpla"];

selected_references = [iscas; epfl; mcnc]; % or any arbitrary list
m =  length(selected_references); % Number of reference designs

kk = 1;
for k = 1 : length(selected_references)
    filename = selected_references(k);
    address = strcat(strcat(strcat(directory,'ReferenceBenchmarks/'),filename),'.stats');
    ccircStats2
    PadStats
end

clear iscas epfl mcnc selected_references;


%% Reading previously generated designs (upto Sample_Number-1)
for k = 1 : Sample_Number-1
    address = strcat(strcat(strcat(directory,'newstat'),num2str(k)),'.txt');
    if isfile(address)
        genStats
        PadStats
        m = m + 1;
    end
end
clear k kk;
%depth = depth-1;


RawData = [Nodeall Inputall Outputall POall Edgeall Fanoutall];


%% PCA
% Normalizing
RefSize = zeros(1,m);
for i = 1:m
    RefSize(i) = sum(RawData(i,2:n));
    RawData(i,:) = RawData(i,:)./RefSize(i);
end

% PCA
[coeffALL,score1] = pca(RawData);%,'Rows','complete');



%% Selecting Hypothetical Reference

% point = [0,0,max(score(:,3)),0]; % arbitrary axis maximum

% maximum for each axis is the chosen value
MaxPoint = max(score1); 

dataMax     = (MaxPoint+mean(RawData*coeffALL))/coeffALL;

nodeMax     = dataMax(1:n); 
inMax       = dataMax(n+1:2*n); 
outMax      = dataMax(2*n+1:3*n);
POMax       = dataMax(3*n+1:4*n);
EdgeMax     = dataMax(4*n+1:5*n);
FOMax       = dataMax(5*n+1:6*n);

% dataMin    = (MinPoint+mean(RawData*coeffALL))/coeffALL;
% nodeMin     = dataMin(1:n); 
% inMin       = dataMin(n+1:2*n); 
% outMin      = dataMin(2*n+1:3*n);
% POMin       = dataMin(3*n+1:4*n);
% EdgeMin     = dataMin(4*n+1:5*n);
% FOMin       = dataMin(5*n+1:6*n);

m = 1;
RefData = [nodeMax;inMax;outMax;POMax;EdgeMax;FOMax]; % V2
% RefData = [Nodeall;Inputall;Outputall;POall;Edgeall;Fanoutall]; % V1

% De-normalizing
RefData = RefData .* nodeSize;

%% Feasibility Losing issue
exitflag = 5;
attemptcount = 1;
while attemptcount <= 10
    
    %% CONSTRAINTS

    %% NODE DISTRIBUTION CONSTRAINT %%
    % Rule1
    Aeq1 = zeros(1,3*n^2+7*n+18*m*n);
    Aeq1(1) = 1; % N
    Beq1(1) = Inputs; % User-input

    % Rule2
    Aeq2 = zeros(1,3*n^2+7*n+18*m*n);
    Aeq2(2:n) = 1; % N
    Beq2(1) = nodeSize; % U-I

    % Rule3
    Ain3 = zeros(n-1,3*n^2+7*n+18*m*n);
    Ain3(1:n-1,2:n) = -maxFanin*triu(ones(n-1,n-1)); % N
    Ain3(1:n-1,1:n) = [eye(n-1) zeros(n-1,1)] + Ain3(1:n-1,1:n); % N
    Bin3 = zeros(n-1,1);

    % Rule4
    Ain4 = zeros(n-1,3*n^2+7*n+18*m*n);
    Ain4(1:n-1,2:n) = eye(n-1); % N
    Ain4(1:n-1,1:n-1) = Ain4(1:n-1,1:n-1) -maxFanout*tril(ones(n-1,n-1)); % N
    Bin4 = zeros(n-1,1);


    %% DESTINATION (INPUT) CONSTRAINT %%
    % Rule5
    Aeq5 = zeros(n,3*n^2+7*n+18*m*n);
    Aeq5(1:n,n+1:2*n) = -eye(n); % I
    for i = 1:n
        for j = 1:i
            Aeq5(i, 6*n + (j-1)*n +i -(j-1) ) = 1; % ET
        end
    end
    Beq5 = zeros(n,1);

    % Rule6
    Ain6 = zeros(n-1,3*n^2+7*n+18*m*n);
    Ain6(1:n-1,2:n) = -maxFanin*eye(n-1); % N
    Ain6(1:n-1,n+2:n+n) = eye(n-1); % I
    Bin6 = zeros(n-1,1);


    %% SOURCE (OUTPUT) CONSTRAINT %%
    % Rule7
    Aeq7 = zeros(n,3*n^2+7*n+18*m*n);
    Aeq7(1:n, 2*n+1:2*n+n) = -eye(n); % O
    Aeq7(1:n, 6*n + 1 : 6*n + n^2) = imresize(eye(n), [n n^2], 'nearest'); % ET
    Beq7 = zeros(n,1);

    % Rule8
    Ain8 = zeros(n,3*n^2+7*n+18*m*n);
    Ain8(1:n, 2*n+1:2*n+n) = eye(n); % O
    Ain8(1:n, 1:n) = -maxFanout*eye(n); % N
    Ain8(1:n,3*n+1:3*n+n) = eye(n); % PO
    Bin8 = zeros(n,1);

    % Rule9
    Ain9 = zeros(n,3*n^2+7*n+18*m*n);
    Ain9(1:n,1:n) = eye(n); % N
    Ain9(1:n,2*n+1 : 2*n+n) = -eye(n); % O
    Ain9(1:n,3*n+1 : 3*n+n) = -eye(n); % PO
    Bin9 = zeros(n,1);


    %% PO Constraints %%
    % Rule10
    Aeq10 = [zeros(1,3*n) ones(1,n) zeros(1,-4*n+3*n^2+7*n+18*m*n)]; % PO
    Beq10 =  Outputs; % U-I

    % Rule11
    Aeq11 = [zeros(1,n-1) 1 zeros(1,3*n -1) -1 zeros(1,2*n) zeros(1,-6*n+3*n^2+7*n+18*m*n)]; % N, PO
    Beq11 =  zeros(1,1);

    %% Edge Table Calculation %%
    % Rule12
    Aeq12 = zeros(n,3*n^2+7*n+18*m*n);
    for i = 1:n
        Aeq12(1:n,6*n+(i-1)*n+1:6*n+(i-1)*n+n)= eye(n); % ET
    end
    Aeq12(1:n,4*n+1:4*n+n) = -eye(n); % E
    Beq12 = zeros(n,1);

    % Rule13
    Ain13 = zeros(n,3*n^2+7*n+18*m*n);
    for i = 1:n-1
        Ain13 (i, 6*n + (i-1)*n + 2) = -1; % ET 
    end
    Ain13(1:n-1,2:n) = eye(n-1); % N
    Bin13 = zeros(n,1);

    % Rule14
    Aeq14 = zeros(1,3*n^2+7*n+18*m*n);
    for j = 1:n
        Aeq14(6*n + (j-1)*n +1) = 1; % ET
    end
    Beq14 = zeros(1,1);

    % Rule15
    Aeq15 = zeros(1,3*n^2+7*n+18*m*n);
    Aeq15( 6*n + n*(n-1)+1 : 6*n + n^2) = 1; % ET
    Beq15 = zeros(1,1);

    % Rule16
    Aeq16 = zeros(1,3*n^2+7*n+18*m*n);
    for j = 2:n
        for k = 1:j-1
            Aeq16(6*n + j*n-j+k+1) = 1; % ET
        end
    end
    Beq16 = zeros(1,1);

    %% Fanout Table Calculation %%
    % Rule17
    Aeq17 = zeros(n,3*n^2+7*n+18*m*n);
    for i = 1:n
        Aeq17(i,n^2 + (6+i-1)*n + 1 : n^2 + (6+i-1)*n + maxFanout +1) = ones(1,maxFanout+1) ; % FoT
    end
    Aeq17(1:n,1:n) = -eye(n); % N
    Beq17 = zeros(n, 1);

    % Rule18
    Aeq18 = zeros(n,3*n^2+7*n+18*m*n);
    for i = 1:n
        Aeq18(i,n^2 + (6+i-1)*n + 1 : n^2 + (6+i-1)*n + maxFanout +1) = 0 : maxFanout ;  % FoT
        Aeq18(i,2*n+i) = -1; % O
    end
    Beq18 = zeros(n, 1);

    % Rule19
    Aeq19 = zeros(1,3*n^2+7*n+18*m*n);
    for i = 1:n
        Aeq19( n^2 + (6+i-1)*n + maxFanout +2 : n^2 + (6+i)*n ) = ones(1,n-maxFanout-1) ; % FoT
    end
    Beq19 = zeros(1, 1);

    % Rule20
    Aeq20 = zeros(n,3*n^2+7*n+18*m*n);
    for i=1:n
        for j = 1 : n
            Aeq20(j,n^2 + (6+i-1)*n+j) = 1;  % FoT
        end
    end
    Aeq20(1:n,5*n+1:5*n+n) = -eye(n); % F
    Beq20 = zeros(n, 1);

    % Rule21
    Ain21 = zeros(1,3*n^2+7*n+18*m*n);
    for i=1:n
        Ain21(n^2 + (6+i-1)*n +1) = 1;  % FoT
    end
    Bin21 = Outputs; % U-I


    %% Optional: Non-equal consecutive data %%
    % Rule22
    % Nodei /= Nodei+1
    q = 1000;
    Anoteq1 = zeros(n-1,3*n^2+7*n+18*m*n);
    Bnoteq1 = zeros(n-1,1);
    Anoteq2 = zeros(n-1,3*n^2+7*n+18*m*n);
    Bnoteq2 = zeros(n-1,1);
    for i=1:n-1
        Anoteq1(i,i) = -1; % N
        Anoteq1(i,i+1) = 1; % N
        Anoteq1(i,3*n^2+6*n+i) = -q; % Non-eq param
        Bnoteq1(i,1) = -1;

        Anoteq2(i,i) = 1; % N
        Anoteq2(i,i+1) = -1; % N
        Anoteq2(i,3*n^2+6*n+i) = q; % Non-eq param
        Bnoteq2(i,1) = q-1;
    end
    Ain22 = [Anoteq1
        Anoteq2];
    Bin22 = [Bnoteq1
        Bnoteq2];
    clear q Anoteq1 Anoteq2 Bnoteq1 Bnoteq2;


    %% Difference
    % Rule23
    Aeq23 = zeros(m*n,3*n^2+7*n+18*m*n);
    for p=0:5
        for j=0:m-1
            Aeq23(p*m*n +j*n+1:p*m*n +j*n+n, p*n +1 : p*n +n) = eye(n); % N,I,O,PO,E,F
            Aeq23(p*m*n +j*n+1:p*m*n +j*n+n, 3*n^2 +(7 + p*m +j)*n +1 : 3*n^2 +(7 + p*m +j)*n +n) = -eye(n); % Diff(N,I,O,PO,E,F)
            Beq23(p*m*n +j*n+1:p*m*n +(j+1)*n,1) = round(transpose(RefData(p*m+j+1,:))); % Ref
        end
    end
    Aeq23 = sparse(Aeq23);
    Beq23 = sparse(Beq23);

    %% Absolute
    % Rule24
    optiMax(1) = nodeSize -n;             % maximum Nodes per depth   in optimal design
    optiMax(2) = (nodeSize -n)*maxFanin;  % maximum Inputs per depth  in optimal design
    optiMax(3) = (nodeSize -n)*maxFanout; % maximum Outputs per depth in optimal design
    optiMax(4) = Outputs;                 % maximum POs per depth     in optimal design
    optiMax(5) = nodeSize*maxFanout;      % maximum Edges per length  in optimal design
    optiMax(6) = nodeSize;                % maximum Fanouts per type  in optimal design
    for p=0:5
        for j=0:m-1
            Aabs1 = zeros(n,3*n^2+7*n+18*m*n);
            Aabs2 = zeros(n,3*n^2+7*n+18*m*n);
            Aabs3 = zeros(n,3*n^2+7*n+18*m*n);
            Aabs4 = zeros(n,3*n^2+7*n+18*m*n);
            Babs4 = zeros(n,1);
            Aabs1(1:n, 3*n^2 +7*n +    p *m*n +j*n +1:3*n^2 +7*n +    p *m*n +j*n +n) =  eye(n); % Diff(N,I,O,PO,E,F)
            Aabs1(1:n, 3*n^2 +7*n + (6+p)*m*n +j*n +1:3*n^2 +7*n + (6+p)*m*n +j*n +n) = -eye(n); % abs(Diff(N,I,O,PO,E,F))
            Aabs2(1:n, 3*n^2 +7*n +    p *m*n +j*n +1:3*n^2 +7*n +    p *m*n +j*n +n) = -eye(n); % Diff(N,I,O,PO,E,F)
            Aabs2(1:n, 3*n^2 +7*n + (6+p)*m*n +j*n +1:3*n^2 +7*n + (6+p)*m*n +j*n +n) = -eye(n); % abs(Diff(N,I,O,PO,E,F))
            Aabs3(1:n, 3*n^2 +7*n +    p *m*n +j*n +1:3*n^2 +7*n +    p *m*n +j*n +n) = -eye(n); % Diff(N,I,O,PO,E,F)
            Aabs3(1:n, 3*n^2 +7*n + (6+p)*m*n +j*n +1:3*n^2 +7*n + (6+p)*m*n +j*n +n) =  eye(n); % abs(Diff(N,I,O,PO,E,F))
            Aabs4(1:n, 3*n^2 +7*n +    p *m*n +j*n +1:3*n^2 +7*n +    p *m*n +j*n +n) =  eye(n); % Diff(N,I,O,PO,E,F)
            Aabs4(1:n, 3*n^2 +7*n + (6+p)*m*n +j*n +1:3*n^2 +7*n + (6+p)*m*n +j*n +n) =  eye(n); % abs(Diff(N,I,O,PO,E,F))
            for i = 1:n
                M = 4*max(abs(RefData(p*m+j+1,i)),optiMax(p+1));
                Aabs3(i, 3*n^2 +7*n +(12+p)*m*n +j*n +i) = -M; % binary param
                Aabs4(i, 3*n^2 +7*n +(12+p)*m*n +j*n +i) =  M; % binary param
                Babs4(i, 1) = M;
            end            
            Ain24(p*4*m*n+j*4*n+1 : p*4*m*n+j*4*n+4*n , :) = [Aabs1; Aabs2; Aabs3; Aabs4];
            Bin24(p*4*m*n+j*4*n+1 : p*4*m*n+j*4*n+4*n , 1) = [zeros(n,1); zeros(n,1); zeros(n,1); Babs4];
        end
    end
    clear Aabs1 Aabs2 Aabs3 Aabs4 Babs4;


    %% Fanin Table
    % Rule25
    Ain25a = zeros(n,3*n^2+7*n+18*m*n);
    for i=2:n
        Ain25a(i, 2*n^2+6*n+(i-1)*n+1 : 2*n^2+6*n+i*n) = -ones(1,n); % FiT
        Ain25a(i, i) = 1; % N
    end
    Bin25a = zeros(n,1);
    Aeq25b = zeros(2*n,3*n^2+7*n+18*m*n);
    for i=2:n    
        Aeq25b(i, 2*n^2+6*n+(i-1)*n+1 : 2*n^2+6*n+(i-1)*n+maxFanin) = 1:maxFanin; % FiT
        Aeq25b(i, n+i) = -1; % I
    end
    Aeq25b(n+1, 2*n^2+6*n+1 : 2*n^2+6*n+n)= ones(1,n);
    for i=2:n    
        Aeq25b(n+i, 2*n^2+6*n+(i-1)*n+maxFanin+1 : 2*n^2+6*n+i*n) = ones(1,(n-maxFanin)); % FiT
    end
    Beq25b = zeros(2*n,1);

    %% Edge Redundancy control
    % Rule26
    % Edge_i_j <= Node_i * Node_(i+j) % non-linear inequality
    % x(6*n+(i-1)*n+1+j) <= x(i)*x(i+j)
    Ain26 = zeros(n^2,3*n^2+7*n+18*m*n);
    for i = 1:n
    %     for j = 1:n-1
    %         Ain26((i-1)*n+j,6*n+(i-1)*n+1+j) = 1; % ET
    % %         Ain26((i-1)*n+j,i)   = -1;  % N
    %         Ain26((i-1)*n+j,i+j) = -1; % N % Addition, not multiplication
    %     end
        Ain26((i-1)*n+1:(i-1)*n+n-1,6*n+(i-1)*n+2:6*n+i*n) = eye(n-1); % ET
        Ain26((i-1)*n+1:(i-1)*n+n-1,i+1:i+n-1) = -eye(n-1); % N
    end
    Bin26 = zeros(n^2, 1);
    % This constraint is erroneous and insufficient. Need Fixing 

    %% OPTIMIZATION FORMULATION
     f=[zeros(1,3*n^2 +7*n + 6*m*n) ones(1,6*m*n) zeros(1,6*m*n)];
    % f = [];
    intcon = (1:3*n^2+7*n+18*m*n);
    optimalLimits = [imresize(optiMax,[1 6*n],'nearest') imresize(optiMax(5),[1 3*n^2],'nearest')];
    ub = [optimalLimits ones(1,n) inf*ones(1,12*m*n) ones(1,6*m*n)];
    lb = [ones(1,n) zeros(1,3*n^2 + 6*n) -inf*ones(1,6*m*n) zeros(1,12*m*n)];
    X0 = [];
    clear optimalLimits optiMax;

    Ain = [
        Ain13 % Edge
        Ain3  % Node
        Ain4  % Node
        Ain6  % Input
        Ain8  % Output
        Ain9  % Output
        Ain21 % Fanout
        Ain22 % =!
        Ain24 % Absolute
        Ain25a % Fanin table
        Ain26 % Uniqueness
        ];
    Bin = [
        Bin13 % Edge
        Bin3  % Node
        Bin4  % Node
        Bin6  % Input
        Bin8  % Output
        Bin9  % Output
        Bin21 % Fanout
        Bin22 % =!
        Bin24 % Absolute
        Bin25a % Fanin table
        Bin26 % Uniqueness
        ];
    Aeq = [
        Aeq12 % Edge
        Aeq1  % Node
        Aeq2  % Node
        Aeq5  % Input
        Aeq7  % Output
        Aeq10 % PO
        Aeq11 % PO
        Aeq14 % Edge
        Aeq15 % Edge
        Aeq16 % Edge
        Aeq17 % Fanout
        Aeq18 % Fanout
        Aeq19 % Fanout
        Aeq20 % Fanout
        Aeq23 % Difference
        Aeq25b % Fanin table
        ];
    Beq = [
        Beq12 % Edge
        Beq1  % Node
        Beq2  % Node
        Beq5  % Input
        Beq7  % Output
        Beq10 % PO
        Beq11 % PO
        Beq14 % Edge
        Beq15 % Edge
        Beq16 % Edge
        Beq17 % Fanout
        Beq18 % Fanout
        Beq19 % Fanout
        Beq20 % Fanout
        Beq23 % Difference
        Beq25b % Fanin table
        ];

    clear Ain13 Ain3 Ain4 Ain6 Ain8 Ain9 Ain21 Ain22 Ain24 Ain25a Ain26;
    clear Bin13 Bin3 Bin4 Bin6 Bin8 Bin9 Bin21 Bin22 Bin24 Bin25a Bin26;
    clear Aeq12 Aeq1 Aeq2 Aeq5 Aeq7 Aeq10 Aeq11 Aeq14 Aeq15 Aeq16 Aeq17 Aeq18 Aeq19 Aeq20 Aeq25b Aeq23;
    clear Beq12 Beq1 Beq2 Beq5 Beq7 Beq10 Beq11 Beq14 Beq15 Beq16 Beq17 Beq18 Beq19 Beq20 Beq25b Beq23;


    % for p=0:5
    %     Difference = transpose(round(solution(3*n^2 +25*n +    p *m*n +1 : 3*n^2 +25*n +  (1+p)*m*n)))
    %     Absolute   = transpose(round(solution(3*n^2 +25*n + (6+p)*m*n +1 : 3*n^2 +25*n +  (7+p)*m*n)))
    %     BinaryCnst = transpose(round(solution(3*n^2 +25*n +(12+p)*m*n +1 : 3*n^2 +25*n + (13+p)*m*n)))
    % end



    %% OPTIMIZATION %%
    % X = intlinprog(f,intcon,A,b,Aeq,beq,LB,UB,X0)

    options = optimoptions('intlinprog','MaxTime',Timeout);%,'PlotFcn',@optimplotmilp);%,'IntegerTolerance',1e-3,'ConstraintTolerance',1e-3,'Heuristics','advanced');%,'Heuristics','round');

    tic
    [solution,fval,exitflag] =  intlinprog(f*MinMax,intcon,Ain,Bin,Aeq,Beq,lb,ub,X0,options);
    elapsedTime = toc


    if exitflag == -2
        disp("Generation is not feasible with current selection of parameters");
    end
    if exitflag ~= -9
        break
    end    
    if exitflag == -9
        disp("Optimizer lost feasibility with current hypothetical point. Moving hypothetical point to double distance.");
        disp("Attempt %d",attempcount);
        RefData = RefData * 2;
    end
%     if attemptcount == 10
%         break
%     end
    attemptcount = attemptcount+1;
end



%% Parsing the results %%
if size(solution)>0
    EdgeTable = zeros(n,n);
    FanoutTable = zeros(n,n);
    FaninTable = zeros(n,n);
    for i =1:n
        EdgeTable(i,:) = round(solution(6*n +(i-1)*n+1 : 6*n +(i-1)*n+n));
        FanoutTable(i,:) = round(solution(n^2 + 6*n +(i-1)*n +1 : n^2 + 6*n +(i-1)*n +n));
        FaninTable(i,:) = round(solution(2*n^2 + 6*n +(i-1)*n +1 : 2*n^2 + 6*n +(i-1)*n +n));
    end

    newNode   = round(solution(    1 :   n));
    newInput  = round(solution(  n+1 : 2*n));
    newOutput = round(solution(2*n+1 : 3*n));
    newPO     = round(solution(3*n+1 : 4*n));
    newEdge   = round(solution(4*n+1 : 5*n));
    newFanout = round(solution(5*n+1 : 6*n));


    %% Plots %%
    if showplot
        DispRefData = zeros(6,n);
        for p = 1:6
            if m==1
                DispRefData(p,:) = RefData(p,:); 
            else
                DispRefData(p,:) = round(mean(RefData((p-1)*m+1:p*m,:)));
            end
        end
    %     n1 = max([DispRefData(1,:) transpose(newNode)])+50;
    %     n2 = max([DispRefData(2,:) transpose(newInput)])+50;
    %     n3 = max([DispRefData(3,:) transpose(newOutput)])+50;
    %     n4 = max([DispRefData(4,:) transpose(newPO)])+3;
    %     n5 = 10;
    %     n6 = max([DispRefData(6,:) transpose(newFanout)])+50;

        plotName = strcat(strcat(strcat(strcat(strcat(directory,num2str(Sample_Number)),'_Gates='),num2str(nodeSize)),',Depth='),num2str(depth-1));
        fig = figure('Name',strcat(num2str(Sample_Number),strcat(strcat(strcat('_Gates=',num2str(nodeSize)),',Depth='),num2str(depth-1))));

        subplot(3,6,1),bar(round(mean(Nodeall)));       ylabel("Gates");    xlabel("depth");    title("Avg. Ref. Node");    %axis([0 n 0 n1]);
        subplot(3,6,2),bar(round(mean(Inputall)));      ylabel("Input");    xlabel("depth");    title("Avg. Ref. Input");   %axis([0 n 0 n2]);
        subplot(3,6,3),bar(round(mean(Outputall)));     ylabel("Output");   xlabel("depth");    title("Avg. Ref. Output");  %axis([0 n 0 n3]);
        subplot(3,6,4),bar(round(mean(POall)));         ylabel("PO");       xlabel("depth");    title("Avg. Ref. PO");      %axis([0 n 0 n4]);
        subplot(3,6,5),bar(log(round(mean(Edgeall))));  ylabel("log(Wires)"); xlabel("wire length"); title("Avg. Ref. Edge"); %axis([0 n 0 n5]);
        subplot(3,6,6),bar(round(mean(Fanoutall))); ylabel("Gates");    xlabel("Fanout");   title("Avg. Ref. Fanout");  %axis([0 n 0 n6]);
        subplot(3,6,7),bar(DispRefData(1,:));   ylabel("Gates");    xlabel("depth");    title("Hypothetical Node");    %axis([0 n 0 n1]);
        subplot(3,6,8),bar(DispRefData(2,:));   ylabel("Input");    xlabel("depth");    title("Hypothetical Input");   %axis([0 n 0 n2]);
        subplot(3,6,9),bar(DispRefData(3,:));   ylabel("Output");   xlabel("depth");    title("Hypothetical Output");  %axis([0 n 0 n3]);
        subplot(3,6,10),bar(DispRefData(4,:));  ylabel("PO");       xlabel("depth");    title("Hypothetical PO");      %axis([0 n 0 n4]);
        subplot(3,6,11),bar(log(DispRefData(5,:))); ylabel("log(Wires)"); xlabel("wire length"); title("Hypothetical Edge"); %axis([0 n 0 n5]);
        subplot(3,6,12),bar(DispRefData(6,:));  ylabel("Gates");    xlabel("Fanout");   title("Hypothetical Fanout");  %axis([0 n 0 n6]);
        subplot(3,6,13),bar(newNode);           ylabel("Gates");    xlabel("depth");    title("New Node");          %axis([0 n 0 n1]);
        subplot(3,6,14),bar(newInput);          ylabel("Input");    xlabel("depth");    title("New Input");         %axis([0 n 0 n2]);
        subplot(3,6,15),bar(newOutput);         ylabel("Output");   xlabel("depth");    title("New Output");        %axis([0 n 0 n3]);
        subplot(3,6,16),bar(newPO);             ylabel("PO");       xlabel("depth");    title("New PO");            %axis([0 n 0 n4]);
        subplot(3,6,17),bar(log(newEdge));      ylabel("log(Wires)"); xlabel("wire length"); title("New Edge");     %axis([0 n 0 n5]);
        subplot(3,6,18),bar(newFanout);         ylabel("Gates");    xlabel("Fanout");   title("New Fanout");        %axis([0 n 0 n6]);
        set(fig,'Position',[0 1000 1080 550]);

        saveas(fig,strcat(plotName,'.epsc'));
        saveas(fig,strcat(plotName,'.bmp'));
    %     clear n1 n2 n3 n4 n5 n6;
    clear DispRefData;
    end

    %% Writing new data %%
    printString = '%s';
    for i = 1:length(newNode)
        printString = strcat(printString,' %d');
    end
    printString = strcat(printString,'\n');
    outfilename = strcat(strcat(strcat(strcat(strcat(strcat(directory,num2str(Sample_Number)),'_Stat_'),num2str(nodeSize)),'_'),num2str(depth-1)),'.txt');
    fileID = fopen(outfilename,'w');

    fprintf(fileID,printString,'Node ',newNode);
    fprintf(fileID,printString,'Input ',newInput);
    fprintf(fileID,printString,'Output ',newOutput);
    fprintf(fileID,printString,'Latched ',zeros(1,n));
    fprintf(fileID,printString,'PO ',newPO);
    fprintf(fileID,printString,'Edge ',newEdge);
    fprintf(fileID,printString,'Fanout ',newFanout);
    fprintf(fileID,'\n');
    fprintf(fileID,'%s %1000.2f \n','fval ',fval);
    fprintf(fileID,'\n');
    fprintf(fileID,'EdgeTable \n');
    for i=1:n
        fprintf(fileID,printString,'',EdgeTable(i,:));
    end
    fprintf(fileID,'\n');
    fprintf(fileID,'FanoutTable \n');
    for i=1:n
        fprintf(fileID,printString,'',FanoutTable(i,:));
    end
    fprintf(fileID,'\n');
    fprintf(fileID,'FaninTable \n');
    for i=1:n
        fprintf(fileID,printString,'',FaninTable(i,:));
    end
    fprintf(fileID,'\n');
    fprintf(fileID,'Sample = %d, Min/Max = %d, Size = %d, Depth = %d, PI = %d, PO = %d, mFI = %d, mFO = %d, timeLimit(s) = %d, optimization time(s) = %d\n',Sample_Number,MinMax,nodeSize,depth-1,Inputs,Outputs,maxFanin,maxFanout,Timeout,elapsedTime);
    nameOfCode = mfilename('fullpath');
    fprintf(fileID,'\n');
    fprintf(fileID,'File generated from %s\n',nameOfCode);
    statFile =  strcat(strcat(strcat(directory,'newstat'),num2str(Sample_Number)),'.txt');
    fileID2 = fopen(statFile,'w');
    copyfile(outfilename,statFile,'f');
    fclose(fileID); fclose(fileID2);
end

%% Clearing Variables %%
clear i j k kk plotName M wireub diffmax p M f intcon lb ub X0;
clear directory outfilename printstring;

% SynthGenV2_3_show_steps_4


