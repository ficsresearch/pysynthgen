#  Copyright (c) University of Florida 2022.
#
#  Author: Daniel Capecci, Princess Lyons
#
#  This file is a part of PySynthGen - a python port of SynthGen by Sarah Amir.
#
#  PySynthGen is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import itertools
import re
import time
from pathlib import Path
import scipy.sparse
import scipy.io
import tqdm
import numpy as np
from sklearn.decomposition import PCA
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
from .common import Sense, Benchmarks, ReferenceStats, padStats, SolutionStats


###############################################################
# Definition
###############################################################
#                    1 :         n           = newNode
#          n        +1 :        2n           = newInput
#         2n        +1 :        3n           = newOutput
#         3n        +1 :        4n           = newPO
#         4n        +1 :        5n           = newEdge
#         5n        +1 :        6n           = newFanout
#         6n        +1 :  n^2 + 6n           = Edge Table
#   n^2 + 6n        +1 : 2n^2 + 6n           = Fanout Table
#  2n^2 + 6n        +1 : 3n^2 + 6n           = Fanin Table
#  3n^2 + 6n        +1 : 3n^2 + 7n           = Non-equality binary parameters
#  3n^2 + 7n +      +1 : 3n^2 + 7n +   mn    = Difference(Node(1:m))
#  3n^2 + 7n +   mn +1 : 3n^2 + 7n +  2mn    = Difference(Input(1:m))
#  3n^2 + 7n +  2mn +1 : 3n^2 + 7n +  3mn    = Difference(Output(1:m))
#  3n^2 + 7n +  3mn +1 : 3n^2 + 7n +  4mn    = Difference(PO(1:m))
#  3n^2 + 7n +  4mn +1 : 3n^2 + 7n +  5mn    = Difference(Edge(1:m))
#  3n^2 + 7n +  5mn +1 : 3n^2 + 7n +  6mn    = Difference(Fanout(1:m))
#  3n^2 + 7n +  6mn +1 : 3n^2 + 7n +  7mn    = Absolute(Difference(Node(1:m)))
#  3n^2 + 7n +  7mn +1 : 3n^2 + 7n +  8mn    = Absolute(Difference(Input(1:m)))
#  3n^2 + 7n +  8mn +1 : 3n^2 + 7n +  9mn    = Absolute(Difference(Output(1:m)))
#  3n^2 + 7n +  9mn +1 : 3n^2 + 7n + 10mn    = Absolute(Difference(PO(1:m)))
#  3n^2 + 7n + 10mn +1 : 3n^2 + 7n + 11mn    = Absolute(Difference(Edge(1:m)))
#  3n^2 + 7n + 11mn +1 : 3n^2 + 7n + 12mn    = Absolute(Difference(Fanout(1:m)))
#  3n^2 + 7n + 12mn +1 : 3n^2 + 7n + 13mn    = Absolution binary parameter(Node(1:m))
#  3n^2 + 7n + 13mn +1 : 3n^2 + 7n + 14mn    = Absolution binary parameter(Input(1:m))
#  3n^2 + 7n + 14mn +1 : 3n^2 + 7n + 15mn    = Absolution binary parameter(Output(1:m))
#  3n^2 + 7n + 15mn +1 : 3n^2 + 7n + 16mn    = Absolution binary parameter(PO(1:m))
#  3n^2 + 7n + 16mn +1 : 3n^2 + 7n + 17mn    = Absolution binary parameter(Edge(1:m))
#  3n^2 + 7n + 17mn +1 : 3n^2 + 7n + 18mn    = Absolution binary parameter(Fanout(1:m))

class Timer:

    def __init__(self, msg=None, end='\n', printer=print):
        self._tic = datetime.datetime.now()
        self.printer = printer
        self.elapsedTime = None
        if msg:
            self.printer(msg, end=end)

    def __call__(self, msg: str, end='\n'):
        self.elapsedTime = datetime.datetime.now() - self._tic
        self.printer(f'{msg} ({str(self.elapsedTime)})', end=end)
        self._tic = datetime.datetime.now()

    def print(self, msg, end='\n'):
        self.printer(msg, end=end)


class Synthesizer:

    def __init__(
        self,
        outDir: Path = Path('./synth_output'),
        minMax: Sense = Sense.minimize,
        maxFanIn=3,
        maxFanOut=3,
        debug=False,
        debugMatPath=None,
    ):
        self.outDir = outDir.expanduser().resolve()
        self.minMax = minMax
        self.maxFanIn = maxFanIn
        self.maxFanOut = maxFanOut
        self.debug = debug
        self.refBenchDir = Path(__file__).parent.joinpath('ReferenceBenchmarks')

        self.debugData = scipy.io.loadmat(
            str(Path(__file__).parent / 'tests/debug.mat') if debugMatPath is None else str(debugMatPath)
        ) if self.debug else None

    def _matEq(self, a: np.ndarray, b: np.ndarray):
        return len(np.argwhere(a != b)) == 0

    def _checkDebug(self, a: np.ndarray, key):
        return self._matEq(a, self.debugData[key])

    def rule1(self, eqSize, inputs):
        # Rule 1
        A = np.zeros((eqSize,))
        A[0] = 1
        B = np.array([inputs])
        if self.debug:
            assert self._checkDebug(A, 'Aeq1')
            assert self._checkDebug(B, 'Beq1')
        return A, B

    def rule2(self, eqSize, n, nodeSize):
        # Rule 2
        A = np.zeros((eqSize,))
        A[1:n] = 1.
        B = np.array([nodeSize])
        if self.debug:
            assert self._checkDebug(A, 'Aeq2')
            assert self._checkDebug(B, 'Beq2')
        return A, B

    def rule3(self, eqSize, n, maxFanin):
        # Rule 3
        A = -maxFanin * np.triu(np.ones((n - 1, n))).astype(int)
        np.fill_diagonal(A, 1)
        A = np.block([A, np.zeros((n - 1, eqSize - A.shape[1])).astype(int)])
        B = np.zeros((n - 1,)).astype(int)
        if self.debug:
            assert self._checkDebug(A, 'Ain3')
            assert self._checkDebug(B, 'Bin3')
        return A, B

    def rule4(self, eqSize, n, maxFanout):
        #  Rule4
        A = -maxFanout * np.tril(np.ones((n - 1, eqSize)))
        A[np.triu_indices_from(A)] = 0
        np.fill_diagonal(A, -maxFanout)
        diagIdxs = np.arange(0, n - 1), np.arange(1, n)
        A[diagIdxs] = 1
        B = np.zeros((n - 1,))
        if self.debug:
            assert self._checkDebug(A, 'Ain4')
            assert self._checkDebug(B, 'Bin4')
        return A, B

    # --------------------------------------
    # Destination (INPUT) Constraint
    # --------------------------------------

    def rule5(self, eqSize, n):
        # Rule 5
        A = np.zeros((n, eqSize))
        A[:n + 1, n:2 * n] = np.diag([-1.] * n)
        for i in range(n):
            for j in range(i + 1):
                A[i, 6 * n + j * n + i - j] = 1
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq5')
            assert self._checkDebug(B, 'Beq5')
        return A, B

    def rule6(self, eqSize, n, maxFanin):
        # Rule 6
        A = np.zeros((n - 1, eqSize))
        A[:n, 1:n] = np.diag([-maxFanin] * (n - 1))
        A[:n, n + 1:n + n] = np.eye(n - 1)
        B = np.zeros((n - 1,))
        if self.debug:
            assert self._checkDebug(A, 'Ain6')
            assert self._checkDebug(B, 'Bin6')
        return A, B

    def rule7(self, eqSize, n):
        # Rule 7
        A = np.block([
            np.zeros((n, 2 * n)),
            np.diag([-1] * n)
        ])
        blocks = [A, np.zeros((n, 6 * n - A.shape[1]))]
        for i in range(n):
            a_ = np.zeros((n, n))
            a_[i, :] = np.ones((1, n))
            blocks.append(a_)
        A = np.block(blocks)
        A = np.block([A, np.zeros((n, eqSize - A.shape[1]))]).astype(int)
        B = np.zeros((n,))
        if self.debug:
            assert self._matEq(A, self.debugData['Aeq7'].astype(int))
            assert self._matEq(B, self.debugData['Beq7'].astype(int))
        return A, B

    def rule8(self, eqSize, n, maxFanout):
        # Rule 8
        A = np.zeros((n, eqSize))
        A_ = np.block([np.diag([-maxFanout] * n), np.zeros((n, n)), np.eye(n), np.eye(n)])
        A[:n, :A_.shape[1]] = A_
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Ain8')
            assert self._checkDebug(B, 'Bin8')
        return A, B

    def rule9(self, eqSize, n):
        # Rule 9
        A = np.zeros((n, eqSize)).astype(int)
        A_ = np.block([
            np.eye(n),
            np.zeros((n, n)),
            np.diag([-1] * n),
            np.diag([-1] * n)
        ]).astype(int)
        A[:, :A_.shape[1]] = A_
        B = np.zeros((n,)).astype(int)
        if self.debug:
            assert self._matEq(A, self.debugData['Ain9'].astype(int))
            assert self._matEq(B, self.debugData['Bin9'].astype(int))
        return A, B

    # --------------------------------------
    # PO Constraints
    # --------------------------------------

    def rule10(self, n, outputs, m):
        # Rule 10
        A = np.concatenate(
            [
                np.zeros((3 * n,)),
                np.ones((n,)),
                np.zeros((-4 * n + 3 * n ** 2 + 7 * n + 18 * m * n,))
            ]
        )
        B = np.array([outputs])
        if self.debug:
            assert self._checkDebug(A, 'Aeq10')
            assert self._checkDebug(B, 'Beq10')
        return A, B

    def rule11(self, n, m):
        # Rule 11
        A = np.concatenate(
            [
                np.zeros((n - 1,)),
                np.ones((1,)),
                np.zeros((3 * n - 1,)),
                -np.ones((1,)),
                np.zeros((2 * n,)),
                np.zeros((-6 * n + 3 * n ** 2 + 7 * n + 18 * m * n,))
            ]
        )
        B = np.zeros((1,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq11')
            assert self._checkDebug(B, 'Beq11')
        return A, B

    # --------------------------------------
    # Edge Table Calc
    # --------------------------------------

    def rule12(self, eqSize, n):
        # Rule 12
        A = np.zeros((n, eqSize))
        for i in range(n):
            A[:n, 6 * n + i * n: 6 * n + i * n + n] = np.eye(n)
        A[:n, 4 * n:4 * n + n] = np.diag([-1.] * n)
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq12')
            assert self._checkDebug(B, 'Beq12')
        return A, B

    def rule13(self, eqSize, n):
        # Rule 13
        A = np.zeros((n, eqSize))
        for i in range(n - 1):
            A[i, 6 * n + i * n + 1] = -1
        A[:n - 1, 1:n] = np.eye(n - 1)
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Ain13')
            assert self._checkDebug(B, 'Bin13')
        return A, B

    def rule14(self, eqSize, n):
        # Rule 14
        A = np.zeros((eqSize,))
        for j in range(n):
            A[6 * n + j * n] = 1
        B = np.zeros((1,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq14')
            assert self._checkDebug(B, 'Beq14')
        return A, B

    def rule15(self, eqSize, n):
        # Rule 15
        A = np.zeros((eqSize,))
        A[6 * n + n * (n - 1): 6 * n + n ** 2] = np.ones((n,))
        B = np.zeros((1,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq15')
            assert self._checkDebug(B, 'Beq15')
        return A, B

    def rule16(self, eqSize, n):
        # Rule 16
        A = np.zeros((eqSize,))
        for j in range(2, n + 1):  # j = 2:n
            for k in range(1, j):  # = 1:j-1
                A[6 * n + j * n - j + k] = 1
        B = np.zeros((1,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq16')
            assert self._checkDebug(B, 'Beq16')
        return A, B

    # --------------------------------------
    # Fanout Table Calc
    # --------------------------------------

    def rule17(self, eqSize, n, maxFanout):
        # Rule 17
        A = np.zeros((n, eqSize))
        for i in range(n):
            A[i, n ** 2 + (6 + i) * n: n ** 2 + (6 + i) * n + maxFanout + 1] = np.ones((maxFanout + 1,))
        A[:n, :n] = np.diag([-1.] * n)
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq17')
            assert self._checkDebug(B, 'Beq17')
        return A, B

    def rule18(self, eqSize, n, maxFanout):
        # Rule 18
        A = np.zeros((n, eqSize))
        for i in range(n):
            A[i, n ** 2 + (6 + i) * n: n ** 2 + (6 + i) * n + maxFanout + 1] = np.arange(0, maxFanout + 1)
            A[i, 2 * n + i] = -1
        A = A.astype(int)
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq18')
            assert self._checkDebug(B, 'Beq18')
        return A, B

        # Rule 19

    def rule19(self, eqSize, n, maxFanout):
        A = np.zeros((eqSize,))
        for i in range(n):
            A[n ** 2 + (6 + i) * n + maxFanout + 1: n ** 2 + (6 + i + 1) * n] = np.ones((n - maxFanout - 1,))
        B = np.zeros((1,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq19')
            assert self._checkDebug(B, 'Beq19')
        return A, B

    def rule20(self, eqSize, n):
        # Rule 20
        A = np.zeros((n, eqSize))
        for i in range(n):
            for j in range(n):
                A[j, n ** 2 + (6 + i) * n + j] = 1
        A[:, 5 * n:5 * n + n] = np.diag([-1.] * n)
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq20')
            assert self._checkDebug(B, 'Beq20')
        return A, B

    def rule21(self, eqSize, n, outputs: int):
        # Rule 21
        A = np.zeros((eqSize,))
        for i in range(n):
            A[n ** 2 + (6 + i) * n] = 1
        B = np.array([outputs])
        if self.debug:
            assert self._checkDebug(A, 'Ain21')
            assert self._checkDebug(B, 'Bin21')
        return A, B

    def rule22(self, eqSize, n):
        # Rule 22 : Optional Non-equal consecutive data
        q = 1000
        Anoteq1 = np.zeros((n - 1, eqSize))
        Bnoteq1 = np.zeros((n - 1,))
        Anoteq2 = np.zeros((n - 1, eqSize))
        Bnoteq2 = np.zeros((n - 1,))
        for i in range(n - 1):
            c = 3 * n ** 2 + 6 * n + i  # 2 * n ** 2 + 12 * n + i
            Anoteq1[i, i] = -1
            Anoteq1[i, i + 1] = 1
            Anoteq1[i, c] = -q
            Bnoteq1[i] = -1

            Anoteq2[i, i] = 1
            Anoteq2[i, i + 1] = -1
            Anoteq2[i, c] = q
            Bnoteq2[i] = q - 1
        A = np.block([[Anoteq1], [Anoteq2]]).astype(int)
        B = np.block([Bnoteq1, Bnoteq2]).astype(int)
        if self.debug:
            assert self._matEq(A, self.debugData['Ain22'].astype(int))
            assert self._matEq(B, self.debugData['Bin22'].astype(int).flatten())
        return A, B

    def rule23(self, eqSize, n, m, refData):
        # Rule 23: Difference
        A = np.block([
            np.eye(6 * n),
            np.zeros((6 * n, 3 * n ** 2 + 7 * n - 6 * n)),
            np.diag([-1] * (6 * n))
        ])
        A = np.block([
            A,
            np.zeros((6 * n, eqSize - A.shape[1]))
        ])
        B = np.array([])
        for p in range(6):
            for j in range(m):
                B = np.concatenate((B, np.round(np.transpose(refData[p * m + j, :]))), axis=0)
        if self.debug:
            assert self._checkDebug(A, 'Aeq23')
            # B is dependent on refData which will be different due to numerical solver differences
            # assert B.shape ==  self.debugData['Beq23'].shape
        return A, B

    def rule24(self, eqSize, n, optiMax, refData, m):
        # Rule 24: Absolute
        A = None
        B = None
        for p in range(6):
            for j in range(m):
                Aabs1 = np.zeros((n, eqSize))
                Aabs2 = np.zeros((n, eqSize))
                Aabs3 = np.zeros((n, eqSize))
                Aabs4 = np.zeros((n, eqSize))
                Babs4 = np.zeros((n,))

                # diff(Node_refj)
                # diffNode = (3 * n ** 2 + 25 * n + p * m * n + j * n,
                #             3 * n ** 2 + 25 * n + p * m * n + j * n + n)
                diffNode = (
                    3 * n ** 2 + 7 * n + p * m * n + j * n,
                    3 * n ** 2 + 7 * n + p * m * n + j * n + n
                )
                # abs(diff(Node_refj))
                absDiffNode = (3 * n ** 2 + 7 * n + (6 + p) * m * n + j * n,
                               3 * n ** 2 + 7 * n + (6 + p) * m * n + j * n + n)

                Aabs1[:, diffNode[0]:diffNode[1]] = np.eye(n)
                Aabs1[:, absDiffNode[0]:absDiffNode[1]] = np.diag([-1.] * n)

                Aabs2[:, diffNode[0]:diffNode[1]] = np.diag([-1.] * n)
                Aabs2[:, absDiffNode[0]:absDiffNode[1]] = np.diag([-1.] * n)

                Aabs3[:, diffNode[0]:diffNode[1]] = np.diag([-1.] * n)
                Aabs3[:, absDiffNode[0]:absDiffNode[1]] = np.eye(n)

                Aabs4[:, diffNode[0]:diffNode[1]] = np.eye(n)
                Aabs4[:, absDiffNode[0]:absDiffNode[1]] = np.eye(n)

                for i in range(n):
                    M = 4 * np.max((np.abs(refData[p * m + j, i]), optiMax[p]))
                    Aabs3[i, 3 * n ** 2 + 7 * n + (12 + p) * m * n + j * n + i] = -M
                    Aabs4[i, 3 * n ** 2 + 7 * n + (12 + p) * m * n + j * n + i] = M
                    Babs4[i] = M

                absAll = np.block([[Aabs1], [Aabs2], [Aabs3], [Aabs4]])
                A = absAll if A is None else np.concatenate([A, absAll], axis=0)
                absB = np.concatenate([np.zeros((n,)), np.zeros((n,)), np.zeros((n,)), Babs4], axis=0)
                B = absB if B is None else np.concatenate([B, absB], axis=0)
        # if self.debug:
        # dependence on refData means these matrices are never equal, only checking shape
        # assert A.shape == self.debugData['Ain24'].shape
        # assert B.shape == self.debugData['Bin24'].shape
        return A, B

    def rule25a(self, eqSize, n):
        A = np.zeros((n, eqSize), dtype=int)
        for i in range(1, n):
            colStart = 2 * n ** 2 + 6 * n + i * n
            colEnd = 2 * n ** 2 + 6 * n + (i + 1) * n
            A[i, colStart:colEnd] = -np.ones((n,), dtype=int)
            A[i, i] = 1
        B = np.zeros((n,))
        if self.debug:
            assert self._checkDebug(A, 'Ain25a')
            assert self._checkDebug(B, 'Bin25a')
        return A, B

    def rule25b(self, eqSize, n, maxFanin):
        # Rule 25: Fanin Table
        A = np.zeros((2 * n, eqSize))
        for i in range(1, n):
            colStart = 2 * n ** 2 + 6 * n + i * n
            colEnd = 2 * n ** 2 + 6 * n + (i + 1) * n
            A[i, colStart:colStart + maxFanin] = np.arange(1, maxFanin + 1)
            A[i, n + i] = -1
            A[n + i, colStart + maxFanin: colEnd] = np.ones((1, n - maxFanin))
        A[n, 2 * n ** 2 + 6 * n: 2 * n ** 2 + 6 * n + n] = np.ones((n,))
        B = np.zeros((2 * n,))
        if self.debug:
            assert self._checkDebug(A, 'Aeq25b')
            assert self._checkDebug(B, 'Beq25b')
        return A, B

    def rule26(self, eqSize, n):
        # Rule 26: Non-Linearity Inequality
        A = np.zeros((n ** 2, eqSize))
        for i in range(n):
            A[i * n: i * n + n - 1, 6 * n + i * n + 1: 6 * n + (i + 1) * n] = np.eye(n - 1)
            A[i * n: i * n + n - 1, i + 1:i + n] = np.diag([-1.] * (n - 1))
        B = np.zeros((n ** 2,))
        if self.debug:
            assert self._checkDebug(A, 'Ain26')
            assert self._checkDebug(B, 'Bin26')
        return A, B

    def buildInequalityConstraints(self, eqSize, n, numPrimaryOutputs, m, optiMax, refData):
        inequalityConstraints = [
            self.rule13(eqSize, n),  # Edge
            self.rule3(eqSize, n, maxFanin=self.maxFanIn),  # Node
            self.rule4(eqSize, n, maxFanout=self.maxFanOut),  # Node
            self.rule6(eqSize, n, maxFanin=self.maxFanIn),  # Input
            self.rule8(eqSize, n, maxFanout=self.maxFanOut),  # Output
            self.rule9(eqSize, n),  # Output
            self.rule21(eqSize, n, outputs=numPrimaryOutputs),  # Fanout
            self.rule22(eqSize, n),  # =!
            self.rule24(eqSize=eqSize, n=n, m=m, optiMax=optiMax, refData=refData),  # Absolute
            self.rule25a(eqSize, n),  # Fanin
            self.rule26(eqSize, n)  # Uniqueness
        ]
        Ain = np.block([[a.reshape(1, -1)] if len(a.shape) == 1 else [a] for a, _ in inequalityConstraints])
        Bin = np.block([b for _, b in inequalityConstraints])
        assert Ain.shape[0] == Bin.shape[0]
        del inequalityConstraints
        return Ain, Bin

    def buildEqualityConstraints(self, eqSize, n, numPrimaryInputs, numPrimaryOutputs, numNodes, refData, m):
        equalityConstraints = [
            self.rule12(eqSize, n),  # Edge
            self.rule1(eqSize, numPrimaryInputs),  # Node
            self.rule2(eqSize, n, numNodes),  # Node
            self.rule5(eqSize, n),  # Input
            self.rule7(eqSize, n),  # Output
            self.rule10(n, numPrimaryOutputs, m),  # PO
            self.rule11(n, m),  # PO
            self.rule14(eqSize, n),  # Edge
            self.rule15(eqSize, n),  # Edge
            self.rule16(eqSize, n),  # Edge
            self.rule17(eqSize, n, self.maxFanOut),  # Fanout
            self.rule18(eqSize, n, self.maxFanOut),  # Fanout
            self.rule19(eqSize, n, self.maxFanOut),  # Fanout
            self.rule20(eqSize, n),  # Fanout
            self.rule23(eqSize, n, m, refData),  # Difference
            self.rule25b(eqSize, n, self.maxFanIn),  # Fanin
        ]
        Aeq = np.block([[a.reshape(1, -1)] if len(a.shape) == 1 else [a] for a, _ in equalityConstraints])
        Beq = np.block([b for _, b in equalityConstraints])
        assert Aeq.shape[0] == Beq.shape[0]
        del equalityConstraints
        return Aeq, Beq

    def solve(
        self,
        name: str,
        idx=1,
        numNodes=5000,
        depth=30,
        numPrimaryInputs=200,
        numPrimaryOutputs=100,
        timeout=7200,
        solver='mip',
        includeSynthStats=True

    ):
        """
        Builds constraint matrices, solves linear mixed-integer problem

        Args:
            name (str): name of the output design
            idx (int): idx used to differentiate versions of output designs with same name
            numNodes ():
            depth ():
            numPrimaryInputs ():
            numPrimaryOutputs ():
            timeout ():
            solver (str): MIP-python or Pulp solvers supported (MIP-python tested more)
            includeSynthStats (): whether or not to include previously generated solutions as part of constraints
        """

        if solver == 'mip':
            try:
                import mip
                _INF = mip.INF
            except ImportError:
                print('MIP-python package not found')
                exit(1)
        elif solver == 'pulp':
            try:
                import pulp
                _INF = None
            except ImportError:
                print("Pulp python package not found")
                exit(1)
        else:
            raise Exception(f'Unsupported solver: {solver}')

        totalTime = Timer()
        # Reading Reference

        # First depth represents primary inputs
        depth += 1
        names = Benchmarks.flatNames()

        # ISCAS 0: 10 (11 benchmarks)
        # EPFL 11: 28 (18 benchmarks)
        # MCNC 29: 207 (179 benchmarks)
        refStart = 0
        refEnd = 207

        names = names[refStart:refEnd + 1]
        try:
            # Include existing similar solutions as part of reference constraints to ensure
            # differentiation
            newStats = list((self.outDir / name).glob(f'{name}_[!{idx}].solstats')) if includeSynthStats else []
        except:
            newStats = []
        newStats.sort()
        nRows = len(names) + len(newStats)

        nodeAll, poAll, fanoutAll = np.zeros((nRows, depth)), np.zeros((nRows, depth)), np.zeros((nRows, depth))
        edgeAll, inputAll, outputAll = np.zeros((nRows, depth)), np.zeros((nRows, depth)), np.zeros((nRows, depth))
        latchedAll = np.zeros((nRows, depth))

        with ThreadPool(mp.cpu_count()) as pool:
            filenames = [self.refBenchDir.joinpath(f'{k}.stats') for k in names]
            results = [(i, pool.apply_async(ReferenceStats.fromPath, (p,))) for i, p in enumerate(filenames)]
            for i, res in tqdm.tqdm(results, desc="Padding Ref Stats", total=len(names)):
                stats = res.get()
                (nodeAll[i, :],
                 poAll[i, :],
                 inputAll[i, :],
                 outputAll[i, :],
                 edgeAll[i, :],
                 fanoutAll[i, :],
                 latchedAll[i, :]) = padStats(stats, depth)

        # prevGenStats
        if len(newStats) > 0:
            for j, filename in tqdm.tqdm(enumerate(newStats), desc='Padding Prev Stats', total=len(newStats)):
                if filename.exists():
                    stats = SolutionStats.fromPath(filename)
                    i = len(names) + j
                    (nodeAll[i, :],
                     poAll[i, :],
                     inputAll[i, :],
                     outputAll[i, :],
                     edgeAll[i, :],
                     fanoutAll[i, :],
                     latchedAll[i, :]) = padStats(stats, depth)

        rawData = np.concatenate((
            nodeAll, inputAll, outputAll, poAll, edgeAll, fanoutAll
        ), axis=1)

        # PCA
        n = depth
        rowSums = np.sum(rawData[:, 1:n + 1], axis=1)
        rawData = rawData / rowSums[:, None]

        if self.debug:
            assert self._checkDebug(rawData, 'RawData')

        pca = PCA(svd_solver='full')
        score1 = pca.fit_transform(rawData)
        # Components are already sorted by explained variance
        coeffAll = pca.components_

        maxPoint = np.max(score1, axis=0)

        if self.debug:
            assert self._matEq(maxPoint.astype(int), self.debugData['MaxPoint'].astype(int))

        # Equivalence to MATLAB -- A / B
        dataMax, _, _, _ = np.linalg.lstsq(
            coeffAll,
            maxPoint + np.mean(rawData @ coeffAll.T, axis=0),
            rcond=None
        )

        # Constraints
        depth -= 1

        # Node distribution constraint
        m = 1

        # eqSize = 3 * n ** 2 + 31 * n + 18 * m * n
        eqSize = 3 * n ** 2 + 7 * n + 18 * m * n

        # De-normalizing
        refData = dataMax.reshape(-1, n)
        refData = refData * numNodes

        # Optimization
        optiMax = np.array([
            numNodes - n,
            (numNodes - n) * self.maxFanIn,
            (numNodes - n) * self.maxFanOut,
            numPrimaryOutputs,
            numNodes * self.maxFanOut,
            numNodes
        ])
        if self.debug:
            assert self._checkDebug(optiMax, 'optiMax')

        Ain, Bin = self.buildInequalityConstraints(eqSize, n, numPrimaryOutputs, m, optiMax, refData)
        Aeq, Beq = self.buildEqualityConstraints(eqSize, n, numPrimaryInputs, numPrimaryOutputs, numNodes, refData, m)

        f = np.concatenate([
            np.zeros((3 * n ** 2 + 7 * n + 6 * m * n,)),
            np.ones((6 * m * n,)),
            np.zeros((6 * m * n,))
        ])
        if self.debug:
            assert self._checkDebug(f, 'f')

        optimalLimits = np.concatenate([
            np.array([[x] * n for x in optiMax]).flatten(),
            np.array([[x] * 3 * n ** 2 for x in optiMax[4:5]]).flatten(),
        ])
        if self.debug:
            assert self._checkDebug(optimalLimits, 'optimalLimits')
        optimalLimits = optimalLimits.tolist()

        # --------- MATLAB VERSION ---------------
        # options = optimoptions('intlinprog',
        #                           'MaxTime', optimTimeoutSec,
        #                           'PlotFcn', @ optimplotmilp); %, 'IntegerTolerance', 1e-3, 'ConstraintTolerance', 1e-3, 'Heuristics', 'advanced'); %, 'Heuristics', 'round');
        #
        # [solution, fval, exitflag, output] = intlinprog(f * MinMax, intcon, Ain, Bin, Aeq, Beq, lb, ub, X0, options);
        # elapsedTime = toc
        # --------- MATLAB VERSION ---------------
        # f * MinMax -- coefficient vector
        # intcon -- vector of integer constraints
        # A * x <= b
        # Ain -- Linear inequality constraint matrix
        # Bin -- Linear inequality constraint matrix
        # A * x = b
        # Aeq -- Linear equality constraint matrix
        # Beq -- Linear equality constraint matrix
        # lb -- lower bounds
        # ub -- upper bounds
        # X0 -- initial point
        # options -- timeout, and plots live progress

        ub = list(itertools.chain(
            optimalLimits,
            np.ones((n,)).tolist(),
            [_INF] * (12 * m * n),
            np.ones((6 * m * n,)).tolist()
        ))
        if self.debug:
            assert self._checkDebug(np.array(ub), 'ub')

        lb = list(itertools.chain(
            np.ones((n,)).tolist(),
            np.zeros((3 * n ** 2 + 6 * n,)).tolist(),
            [-_INF] * (6 * m * n),
            np.zeros((12 * m * n,)).tolist()
        ))
        if self.debug:
            assert self._checkDebug(np.array(lb), 'lb')

        bounds = list(zip(lb, ub))

        solution = None
        numConstraints = max(Aeq.shape[0], Ain.shape[0])
        if solver == 'pulp':
            tic = Timer('Running Pulp', end='')
            # PULP
            prob = pulp.LpProblem('Synthesis', pulp.LpMinimize)

            # Variables
            x = [
                pulp.LpVariable(f'x{i}', lowBound=lb, upBound=ub, cat=pulp.LpInteger)
                for i, (lb, ub) in enumerate(bounds)
            ]

            # Objective function
            prob += pulp.lpSum([f[i] * x[i] for i in range(len(f))])

            # Constraints
            for i in tqdm.trange(Aeq.shape[0], desc=f'Adding constraints ({solver})', total=numConstraints):
                if i < Aeq.shape[0]:
                    prob += pulp.LpAffineExpression((x[j], Aeq[i, j]) for j in range(Aeq.shape[1])) == Beq[i]
                if i < Ain.shape[0]:
                    prob += pulp.LpAffineExpression((x[j], Ain[i, j]) for j in range(Ain.shape[1])) <= Bin[i]

            print("\n\nSolving...")
            prob.solve(solver=pulp.PULP_CBC_CMD())  # pulp.GLPK_CMD(msg=True))
            print(prob)
            variables = prob.variablesDict()
            solution = np.array(
                [variables[f'x{i}'].varValue if f'x{i}' in variables.keys() else 0 for i in range(len(f))])
            tic('Done')
        else:

            # MIP
            model = mip.Model(
                sense=mip.MINIMIZE if self.minMax == Sense.minimize else mip.MAXIMIZE,
                solver_name=mip.CBC,
            )

            # Variables
            x = np.array([
                model.add_var(var_type=mip.INTEGER, lb=lb, ub=ub, name=f'x{i}')
                for i, (lb, ub) in enumerate(bounds)
            ]).view(mip.LinExprTensor)

            # Objective function
            objFun = mip.minimize if self.minMax == Sense.minimize else mip.maximize
            model.objective = objFun(
                mip.xsum(
                    [f[i] * x[i] * self.minMax.value for i in range(len(f))]
                )
            )

            # Constraints
            tic = Timer("Adding Equality Constraints...", end="")
            model += (Aeq @ x == Beq)
            tic('Done')

            print("Adding Inequality Constraints...", end="")
            model += (Ain @ x <= Bin)
            tic('Done')

            print("\n\nSolving...")
            # model.verbose = True
            model.optimize(
                max_seconds=timeout
            )
            totalTime("Done. Total Time")
            if model.status == mip.OptimizationStatus.OPTIMAL:
                solution = np.array([v.x for v in model.vars])
            # elif model.status == mip.OptimizationStatus.ERROR:
            #     pass
            # elif model.status == mip.OptimizationStatus.INFEASIBLE:
            #     pass
            # elif model.status == mip.OptimizationStatus.INT_INFEASIBLE:
            #     pass
            # elif model.status == mip.OptimizationStatus.NO_SOLUTION_FOUND:
            #     pass
            # elif model.status == mip.OptimizationStatus.UNBOUNDED:
            #     pass
            else:
                print(f'Solution status {model.status}')

        # TODO: parse / plot results

        if solution is None:
            return None

        solution = solution.reshape(-1, n)
        (newNode, newInput, newOutput, newPO, newEdge, newFanout) = solution[:6, :]
        solution = solution[6:, :]
        edgeTable = solution[:n, :]
        solution = solution[n:, :]
        fanoutTable = solution[:n, :]
        solution = solution[n:, :]
        faninTable = solution[:n, :]

        solStats = SolutionStats(
            nodes=newNode,
            inputs=newInput,
            outputs=newOutput,
            POs=newPO,
            edges=newEdge,
            fanouts=newFanout,
            fanoutTable=fanoutTable,
            faninTable=faninTable,
            edgeTable=edgeTable,
            latched=None,
            meta={
                "sample": idx,
                "min_max": str(self.minMax),
                "nodes": numNodes,
                "depth": depth,
                "primary_inputs": numPrimaryInputs,
                "primary_outputs": numPrimaryOutputs,
                "max_fanin": self.maxFanIn,
                "max_fanout": self.maxFanOut,
                "timeout_sec": timeout,
                "elapsed_time": str(totalTime.elapsedTime),
                "fval": None
            }
        )

        return solStats

    @staticmethod
    def arrayToString(table, maxLineWidth=99999999):
        return re.sub(r'[ ]+', ' ',
                      np.array2string(table.astype(int), max_line_width=maxLineWidth).replace('[', '').replace(']', ''))

    def resultsToString(
        self,
        newNode: np.ndarray,
        newInput: np.ndarray,
        newOutput: np.ndarray,
        newPO: np.ndarray,
        newEdge: np.ndarray,
        newFanout: np.ndarray,
        edgeTable: np.ndarray,
        fanoutTable: np.ndarray,
        faninTable: np.ndarray,
        arguments: dict,
        fval=None,
        latched=None
    ):
        if latched is None:
            latched = np.zeros((1, np.max(newNode.shape)))

        out = f"Node {self.arrayToString(newNode.astype(int))}\n"
        out += f"Input {self.arrayToString(newInput.astype(int))}\n"
        out += f"Output {self.arrayToString(newOutput.astype(int))}\n"
        out += f"Latched {self.arrayToString(latched.astype(int))}\n"
        out += f"PO {self.arrayToString(newPO.astype(int))}\n"
        out += f"Edge {self.arrayToString(newEdge.astype(int))}\n"
        out += f"Fanout {self.arrayToString(newFanout.astype(int))}\n"
        out += f"\nfval {fval if fval else ''}\n\n\n"
        out += f"EdgeTable\n{self.arrayToString(edgeTable.astype(int))}\n\n"
        out += f"FanoutTable\n{self.arrayToString(fanoutTable.astype(int))}\n\n"
        out += f"FaninTable\n{self.arrayToString(faninTable.astype(int))}\n\n\n"

        for k, v in arguments.items():
            out += f'{k} = {v}\n'

        out += f'\nFile generated by PySynthGen © {datetime.datetime.now().year} University of Florida'
        return out


if __name__ == '__main__':
    pass
